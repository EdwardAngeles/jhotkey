package com.angega.jhotkey;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class HotkeyManager implements NativeKeyListener
{
    public List<Hotkey> hotkeys = new ArrayList<>();

    public static void main(String[] args) {new HotkeyManager();}

    public HotkeyManager()
    {
        setupGlobalScreen();
        loadHotkeys();
    }

    private void setupGlobalScreen()
    {
        LogManager.getLogManager().reset();
        Logger logger = Logger.getLogger(GlobalScreen.class.getPackage().getName());
        logger.setLevel(Level.OFF);

        try { GlobalScreen.registerNativeHook();
        } catch (NativeHookException e) {e.printStackTrace();}

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                GlobalScreen.removeNativeKeyListener(this);
                GlobalScreen.unregisterNativeHook();

                Thread.sleep(1000);
                GlobalScreen.removeNativeKeyListener(this);
                System.out.println("\nEnd");
            } catch (Exception e) {e.printStackTrace();}
        }));

        GlobalScreen.addNativeKeyListener(this);
    }

    private void loadHotkeys()
    {
        File folder  = new File(".");
        File[] files = folder.listFiles(file ->
                file.isFile() && file.getName().toLowerCase().endsWith(".json"));
        Arrays.sort(files);

        Gson gson = new Gson();
        JsonReader reader;
        for (File file : files)
        {
            try {
                reader = new JsonReader(new FileReader(file));

                Hotkey hotkey  = gson.fromJson(reader, Hotkey.class);
                hotkey.pressed = new boolean[hotkey.keys.length];
                hotkey.name    = file.getName();

                hotkeys.add(hotkey);

                // printing hotkey...
                String strKeys = Arrays.toString(hotkey.keys).replace(", ", " + ");
                System.out.println("loaded: " + strKeys + " : " + hotkey.name);
            } catch (FileNotFoundException e) {e.printStackTrace();}
        }

        if (hotkeys.size() == 0)
        {
            System.out.println("No files found.");
            System.exit(0);
        }
    }

    @Override
    public void nativeKeyPressed(NativeKeyEvent e)
    {
        String keyPressed = NativeKeyEvent.getKeyText(e.getKeyCode()).toLowerCase();

        for (Hotkey hotkey : hotkeys)
        {
            for (int i = 0; i < hotkey.keys.length; i++)
            {
                if (hotkey.keys[i].equals(keyPressed))
                {
                    hotkey.pressed[i] = true;
                }
            }

            if (hotkey.running == false)
            {
                hotkey.running = true;
                for (Boolean press : hotkey.pressed)
                {
                    if (press == false)
                    {
                        hotkey.running = false;
                        break;
                    }
                }

                if (hotkey.running)
                {
                    System.out.println("fired: " + hotkey.name);
                    ActionRunner.run(hotkey);
                }
            }
        }
    }

    @Override
    public void nativeKeyReleased(NativeKeyEvent e)
    {
        String keyPressed = NativeKeyEvent.getKeyText(e.getKeyCode()).toLowerCase();

        for (Hotkey hotkey : hotkeys)
        {
            for (int i = 0; i < hotkey.keys.length; i++)
            {
                if (hotkey.keys[i].equals(keyPressed))
                {
                    hotkey.pressed[i] = false;
                }
            }
        }
    }

    @Override
    public void nativeKeyTyped(NativeKeyEvent e) {}
}
