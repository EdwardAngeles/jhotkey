package com.angega.jhotkey;


import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

public class ActionRunner
{
    static ActionRobot actionRobot;
    static int xpos;
    static int ypos;

    static {
        try {actionRobot = new ActionRobot();
        } catch (AWTException e) {e.printStackTrace();}
    }

    public static void run(Hotkey hotkey)
    {
        new Thread(() ->
        {
            for (String action : hotkey.actions)
            {
                if (action.startsWith("mouse move: "))
                {
                    String   str = action.replaceAll("[^0-9]+", " ");
                    String[] num = str.trim().split(" ");

                    actionRobot.mouseMove(Integer.parseInt(num[0]), Integer.parseInt(num[1]));
                }

                if (action.startsWith("sleep: "))
                {
                    String str         = action.replace("sleep: ", "");
                    float  seconds     = Float.parseFloat(str);
                    int    miliseconds = (int) (seconds * 1000f);

                    actionRobot.delay(miliseconds);
                    actionRobot.waitForIdle();
                }

                if (action.startsWith("send: "))
                {
                    String str = action.replace("send: ", "");
                    actionRobot.type(str);
                }

                if (action.equals("left click"))
                {
                    actionRobot.mousePress  (InputEvent.BUTTON1_DOWN_MASK);
                    actionRobot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
                }

                if (action.equals("right click"))
                {
                    actionRobot.mousePress  (InputEvent.BUTTON3_DOWN_MASK);
                    actionRobot.mouseRelease(InputEvent.BUTTON3_DOWN_MASK);
                }

                if (action.startsWith("left click: "))
                {
                    String   str = action.replaceAll("[^0-9]+", " ");
                    String[] num = str.trim().split(" ");

                    actionRobot.mouseMove(Integer.parseInt(num[0]), Integer.parseInt(num[1]));
                    actionRobot.mousePress  (InputEvent.BUTTON1_DOWN_MASK);
                    actionRobot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
                }

                if (action.startsWith("right click: "))
                {
                    String   str = action.replaceAll("[^0-9]+", " ");
                    String[] num = str.trim().split(" ");

                    actionRobot.mouseMove(Integer.parseInt(num[0]), Integer.parseInt(num[1]));
                    actionRobot.mousePress  (InputEvent.BUTTON3_DOWN_MASK);
                    actionRobot.mouseRelease(InputEvent.BUTTON3_DOWN_MASK);
                }

                if (action.startsWith("key press: "))
                {
                    try {
                        String str  = action.replace("key press: ", "");
                        int keyCode = KeyEvent.class.getField(str).getInt(null);

                        actionRobot.keyPress(keyCode);
                    } catch (Exception e) {e.printStackTrace();}
                }

                if (action.startsWith("key release: "))
                {
                    try {
                        String str  = action.replace("key release: ", "");
                        int keyCode = KeyEvent.class.getField(str).getInt(null);

                        actionRobot.keyRelease(keyCode);
                    } catch (Exception e) {e.printStackTrace();}
                }

                if (action.startsWith("key type: "))
                {
                    try {
                        String str  = action.replace("key press: ", "");
                        int keyCode = KeyEvent.class.getField(str).getInt(null);

                        actionRobot.keyPress(keyCode);
                        actionRobot.keyRelease(keyCode);
                    } catch (Exception e) {e.printStackTrace();}
                }

                if (action.equals("save pos"))
                {
                    xpos = MouseInfo.getPointerInfo().getLocation().x;
                    ypos = MouseInfo.getPointerInfo().getLocation().y;
                }

                if (action.equals("load pos"))
                {
                    actionRobot.mouseMove(xpos, ypos);
                }
            }
            hotkey.running = false;
        }).start();
    }

}
