package com.angega.jhotkey;

public class Hotkey
{
    public String[] keys    = new String[]{};
    public String[] actions = new String[]{};

    public boolean[] pressed;
    public boolean   running = false;

    public String name;

    @Override
    public String toString() {
        String string = "";

        string = "{key: ";
        for (String key : keys)
        {
            string += key + ", ";
        }
        string += "~";
        string = string.replace(", ~", "");

        string += " ~ actions: ";
        for (String action : actions)
        {
            string += action + ", ";
        }
        string += "~";
        string = string.replace(", ~", "}");

        return string;
    }
}
